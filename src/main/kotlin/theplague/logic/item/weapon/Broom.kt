package theplague.logic.item.weapon

class Broom:Weapon() {
    override var timesLeft: Int = 1

    override fun use() {
        timesLeft--
    }
    override var icon: String = "\uD83E\uDDF9"
}