package theplague.logic.item.weapon

class Sword:Weapon() {
    override var timesLeft: Int = 1

    override fun use() {
        timesLeft--
    }
    override var icon: String ="\uD83D\uDDE1"
}