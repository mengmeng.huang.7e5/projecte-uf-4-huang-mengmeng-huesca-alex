package theplague.logic.item.vehicle

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import kotlin.math.abs

class Bicycle:Vehicle() {
    override fun canMove(from: Position, to: Position): Boolean {
        if (abs(from.x - to.x) <= 4 && abs(from.y - to.y) <= 4 && abs(from.x - to.x) + abs(from.y - to.y) <= 4){
            return true
        }
        return false
    }

    override var timesLeft: Int = 5

    override fun use() {
        timesLeft--
    }
    override var icon: String = "\uD83D\uDEB2"
}