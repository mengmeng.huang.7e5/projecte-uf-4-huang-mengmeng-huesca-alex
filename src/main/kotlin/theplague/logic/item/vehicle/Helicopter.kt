package theplague.logic.item.vehicle

import theplague.interfaces.Position

class Helicopter:Vehicle() {
    override fun canMove(from: Position, to: Position): Boolean {
        return true
    }

    override var timesLeft: Int = 5

    override fun use() {
        timesLeft--
    }
    override var icon: String = " \uD83D\uDE81"
}