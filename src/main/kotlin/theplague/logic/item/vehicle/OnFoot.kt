package theplague.logic.item.vehicle

import theplague.interfaces.Position
import java.lang.Math.abs
import kotlin.math.pow
import kotlin.math.sqrt

class OnFoot: Vehicle() {
    override fun canMove(from: Position, to: Position):Boolean{
        if (abs(from.x - to.x) <= 1 && abs(from.y - to.y) <= 1 && abs(from.x - to.x) + abs(from.y - to.y) <= 2){
            return true
        }
        return false
    }
    override var timesLeft: Int = 1


    override fun use() {
        timesLeft=1
    }
    override var icon: String = "\uD83D\uDEB6"
}