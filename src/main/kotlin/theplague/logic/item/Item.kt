package theplague.logic.item

import theplague.interfaces.Iconizable

abstract class Item:Iconizable{
    abstract var timesLeft: Int
    abstract fun use()
}