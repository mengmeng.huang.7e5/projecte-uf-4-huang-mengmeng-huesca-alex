package theplague.logic

import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.item.vehicle.OnFoot
import theplague.logic.item.vehicle.Vehicle
import theplague.logic.item.weapon.Hand
import theplague.logic.item.weapon.Weapon

class Player( var position:Position ):IPlayer, Iconizable {


    override var currentVehicle: Vehicle = OnFoot()
    override var currentWeapon: Weapon =  Hand()
    override var livesLeft: Int = 15
    override var turns:Int = 0
    override var icon: String = "\uD83D\uDEB6"

    fun incrementTurn() {
        turns+=1
    }
}