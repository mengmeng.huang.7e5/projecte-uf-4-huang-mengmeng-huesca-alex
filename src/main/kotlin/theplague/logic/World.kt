package theplague.logic

import theplague.interfaces.*
import theplague.logic.colony.Ant
import theplague.logic.colony.Colony
import theplague.logic.colony.Dragon
import theplague.logic.item.vehicle.Bicycle
import theplague.logic.item.vehicle.Helicopter
import theplague.logic.item.vehicle.OnFoot
import theplague.logic.item.vehicle.Vehicle
import theplague.logic.item.weapon.Broom
import theplague.logic.item.weapon.Hand
import theplague.logic.item.weapon.Sword
import theplague.logic.item.weapon.Weapon
import java.util.concurrent.ThreadLocalRandom
import kotlin.random.Random

class World(override val width: Int= 10, override val height: Int= 10): IWorld {

    override val territories: List<List<Territory>> =
        List(width) { x -> List(height) { y -> Territory(Position(x, y)) } }

    override val player = Player(Position(width / 2, height / 2))

    var playerTerritory = territories[player.position.x][player.position.y]

    init {
        playerTerritory.hasPlayer = true
        val randomPos = randomPosition()
        territories[randomPos.x][randomPos.y].colony = Ant()
    }

    override fun nextTurn() {
        player.incrementTurn()
        generateNewItems()
        generateNewColonies()
        reproduce()
    }

//    private fun reproduce() {
//        playerTerritory.colony?.reproduce()
//    }

    override fun gameFinished(): Boolean {
        return player.livesLeft == 0
    }

    override fun canMoveTo(position: Position): Boolean {
        return player.currentVehicle.canMove(player.position, position)
    }

    override fun moveTo(position: Position) {
        if (canMoveTo(position)) {
            playerTerritory.hasPlayer = false
            player.position = position
            playerTerritory = territories[player.position.y][player.position.x]
            playerTerritory.hasPlayer = true
            player.currentVehicle.use()
            if (player.currentVehicle.timesLeft == 0) {
                player.currentVehicle = OnFoot()
            }
        }
    }

    override fun exterminate() {
        val colony = playerTerritory.colony
        if (colony != null) {
            playerTerritory.exterminate(player.currentWeapon)
            player.currentWeapon.use()
        }
        if (player.currentWeapon.timesLeft == 0) {
            player.currentWeapon = Hand()
        }

    }

    override fun takeableItem(): Iconizable? {
        return playerTerritory.item
    }

    override fun takeItem() {
        if (playerTerritory.item is Vehicle) {
            player.currentVehicle = playerTerritory.item as Vehicle
            playerTerritory.item = null
        } else if (playerTerritory.item is Weapon) {
            player.currentWeapon = playerTerritory.item as Weapon
            playerTerritory.item = null
        }
    }

    fun randomPosition(): Position {
        val randomWidth = ThreadLocalRandom.current().nextInt(territories.size)
        val randomLenght = ThreadLocalRandom.current().nextInt(territories[randomWidth].size)
        return Position(randomWidth, randomLenght)
    }

    fun generateNewColonies() {
        val randomPos = randomPosition()
        val colony: Colony?
        when (Random.nextInt(1, 100)) {
            in 1..30 -> colony = Ant()
            in 31..40 -> colony = Dragon()
            else -> colony = null
        }
        place(Colonization(colony, randomPos))
    }

    fun place(colonization: Colonization) {
        if (colonization.colony != null) {
            if (territories[colonization.position.x][colonization.position.y].colony == null) {
                territories[colonization.position.x][colonization.position.y].colony = colonization.colony
            } else {
                var colonyWinner =
                    territories[colonization.position.x][colonization.position.y].colony!!.colonizedBy(colonization.colony!!)
                territories[colonization.position.x][colonization.position.y].colony = colonyWinner
            }
        }
    }

    fun generateNewItems() {
        val randomPos = randomPosition()
        when (Random.nextInt(1, 100)) {
            in 1..25 -> {

                territories[randomPos.x][randomPos.y].item = Bicycle()
            }
            in 25..35 -> {
                territories[randomPos.x][randomPos.y].item = Helicopter()
            }
            in 35..60 -> {
                territories[randomPos.x][randomPos.y].item = Broom()
            }
            in 60..70 -> {
                territories[randomPos.x][randomPos.y].item = Sword()
            }
        }
    }


    fun reproduce() {
        territories.flatten().forEach{territory ->  if(territory.colony!=null ) territory.colony!!.reproduce() }
    }

    fun expand() {
        TODO("Not yet implemented")
    }
}
