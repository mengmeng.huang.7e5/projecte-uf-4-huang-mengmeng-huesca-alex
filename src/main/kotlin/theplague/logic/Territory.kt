package theplague.logic

import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.colony.Colony
import theplague.logic.item.Item
import theplague.logic.item.weapon.Weapon

class Territory (val position:Position ):ITerritory{

    override var hasPlayer = false
    var item : Item? = null
    var colony : Colony? = null
    private var plagueSize:Int? = colony?.size

    override fun iconList(): List<Iconizable> {
        val list = mutableListOf<Iconizable>()
        if(hasPlayer){
            list.add(Player(Position(0,0)))
        }
        if (item != null){
            list.add(item!!)
        }
        if (colony != null){
            repeat(colony!!.size){
                list.add(colony!!)
            }
        }
        return list
    }

    fun exterminate(weapon: Weapon){
        colony?.attacked(weapon)
        if (colony?.size!! <= 0){
            colony = null
        }
    }
}