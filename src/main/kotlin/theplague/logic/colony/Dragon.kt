package theplague.logic.colony

import theplague.interfaces.Position
import theplague.logic.Colonization
import theplague.logic.item.weapon.Hand
import theplague.logic.item.weapon.Sword
import theplague.logic.item.weapon.Weapon

class Dragon:Colony() {
    private var turnsToExpand:Int=0
    companion object {
        const val reproduction_time = 5
    }
    override var size: Int = 1

    override fun needsToExpand():Boolean {
        if (size==3){
            return true
        }
        return false
    }



    override fun willReproduce(): Boolean {
        if (size==1){
            turnsToExpand++
            if (turnsToExpand == reproduction_time){
                turnsToExpand=0
                return true
            }
        }
        if (size==2){
            turnsToExpand++
            if (turnsToExpand == reproduction_time){
                turnsToExpand=0
                return true
            }
        }
        if (size==3){
            turnsToExpand++
            if (turnsToExpand == reproduction_time){
                turnsToExpand=0
                return true
            }
        }
        return false
    }

    override fun attacked(weapon: Weapon) {
        when (weapon){
            is Sword-> size--
            else -> size
        }
    }

    override fun colonizedBy(plague: Colony): Colony {
        TODO("Not yet implemented")
    }

    override fun expand(position: Position, maxPosition: Position): List<Colonization> {
        if (needsToExpand()){
            TODO("Not yet implemented")
        }
        TODO("Not yet implemented")
    }

    override var icon: String = "\uD83D\uDC09"
}