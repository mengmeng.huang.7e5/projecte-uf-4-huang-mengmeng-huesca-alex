package theplague.logic.colony

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.Colonization
import theplague.logic.Player
import theplague.logic.item.weapon.Weapon

abstract class Colony:Iconizable {
    abstract var size:Int
    abstract fun willReproduce():Boolean
     fun reproduce() {
        size++
    }
    abstract fun needsToExpand():Boolean
    abstract fun attacked(weapon:Weapon)
    abstract fun colonizedBy(plague:Colony):Colony
    abstract fun expand(position:Position, maxPosition: Position):List<Colonization>
}
