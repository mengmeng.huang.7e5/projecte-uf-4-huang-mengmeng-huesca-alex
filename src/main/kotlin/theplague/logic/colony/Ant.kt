package theplague.logic.colony

import theplague.interfaces.Position
import theplague.logic.Colonization
import theplague.logic.item.weapon.Broom
import theplague.logic.item.weapon.Hand
import theplague.logic.item.weapon.Sword
import theplague.logic.item.weapon.Weapon
import kotlin.random.Random

class Ant() : Colony() {

    private val reproductionTax:Double = 30.0

    override var size: Int = 1

    override fun willReproduce(): Boolean {
        if (Random.nextInt(1,100) in 1..reproductionTax.toInt()) {
            return true
        }
        return false
    }


    override fun needsToExpand():Boolean {
        if (size==3){
            return true
        }
        return false
    }

    override fun attacked(weapon: Weapon) {
        when (weapon){
            is Hand -> size-=2
            is Broom -> size = 0
            is Sword -> size--
        }
    }

    override fun colonizedBy(plague: Colony): Colony {
       var colony: Colony = this
        when(plague) {
           is Ant -> this.size++
           is Dragon -> colony = plague
       }
        return colony
    }

    override fun expand(position: Position, maxPosition: Position): List<Colonization> {
        if (needsToExpand()){
            TODO("Not yet implemented")
        }
        TODO("Not yet implemented")
    }

    override var icon: String = "\uD83D\uDC1C"
}