package theplague.interfaces

interface ITerritory {
    abstract var hasPlayer: Boolean

    /**
     * List of icons of the current territory (4 max)
     */
    fun iconList() : List<Iconizable>
}